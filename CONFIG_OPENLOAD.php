<?php
/*
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

___---::INSTRUCCIONES, FAVOR LEER ATENTAMENTE:.---___

Si no tienes una cuenta en Openload crea una en: https://openload.co/
Una vez tengas una cuenta accede con tus datos como te lo indican, y entra 
a la opcion de "User Settings" donde tendras diferentes paneles de informacion.

Busca el panel con el titulo "API/FTP Access" donde tendras 4 datos:

		- FTP Server:
		- API Server(see API documentation):
		- FTP Username/API Login:
		- FTP Password/API Key: 

De estos datos nos importan los ultimos 2, "FTP Username/API Login" solo lo copiamos y pegamos en
la variable $login que secarga en este archivo. Por otro lado "FTP Password/API Key" para verlo debemos
dar click en el icono del lapiz y nos pediran nuestra clave de cuenta de Openload (con la que entramos).
Al pasar ese dato se recargara la pagina y veremos una franja color verde que dira:

		- Your FTP Password/API Key is: XXXXXXXX

Copiamos nuestra clave y la pegamos en la variable $key que esta en este documento. 

Con esto queda configurado el script, ahora solo queda entrar a donde lo tengamos cargado y usarlo.

/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////

*/


$login = "Pega tu FTP Username/API Login aquí";

$key = "Pega tu FTP Password/API Key aquí";

?>