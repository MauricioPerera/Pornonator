<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"  "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
	<head>
		<title>APPONERLA</title>

		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
	</head>
	<body>

<?php

	require 'simple_html_dom.php';

	$url = 'http://www.fapmenow.com/video/categories/';
	
	$html = file_get_html($url);

	$html_a = $html->find('.categoriesBlock .item a.title');

	$html_b = $html->find('.categoriesBlock .item div.cam');

	$catCount = count($html_a);

	$catOPTIONS = array();

	$data_a = array();

	$data_b = array();

	foreach ($html_a as $link) {

		$catHREF = $link->href;

		$catTEXT = strip_tags($link->plaintext);

		$linkData = array($catTEXT, $catHREF);

		array_push($data_a, $linkData);

	}

		foreach ($html_b as $cantidad) {

		$totalVideos = strip_tags($cantidad->plaintext);

		array_push($data_b, $totalVideos);

	}

	for ($i=0; $i < $catCount; $i++) { 

		$cantidad = $data_b[$i];

		$masdata = $data_a[$i];

		$value = $masdata[1].'----'.$cantidad.'----'.$masdata[0];

		$option = '<option value="'.$value.'">'.$masdata[0].'</option>';

		array_push($catOPTIONS, $option);
	}

?>

		<form action="/exportador.php" method="POST">
		  <div class="form-group">
		    <label for="category">Escoja una categoría de las  <?php echo $catCount; ?> que tenemos</label>
		  	<select id="category" name="category" class="form-control">
		  		<?php
					foreach ($catOPTIONS as $option) {
						echo $option;
					}
				?>
			</select>
		  </div>
		  <button type="submit" name="importar" class="btn btn-default">Importar</button>
		</form>

	</body>
</html>